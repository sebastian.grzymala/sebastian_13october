import {createSlice, PayloadAction} from '@reduxjs/toolkit';

type TOrderList = [number, number][];

type TProduct = 'PI_XBTUSD' | 'PI_ETHUSD' | null;
type TTick = 0.5 | 1 | 2.5;
type TTickSize = 1 | 10;

type TDataMessage = {
  bids: TOrderList;
  asks: TOrderList;
};

export interface DataState {
  bids: TOrderList;
  asks: TOrderList;
  product_id: TProduct;
  tick: TTick;
  tick_size: TTickSize;
}

/*
const initialState: DataState = {
  bids: [
    [56903.5, 167055.0],
    [57228.5, 240944.0],
    [57242.0, 1856.0],
    [57265.0, 10658.0],
    [57265.5, 1728005.0],
    [57389.5, 130000.0],
    [57403.5, 400149.0],
    [57417.5, 11880.0],
    [57424.5, 17249.0],
    [57457.5, 50000.0],
    [57459.0, 5000.0],
    [57462.5, 30000.0],
    [57463.0, 0.0],
    [57464.0, 0.0],
    [57467.5, 10000.0],
    [57477.0, 2500.0],
  ]
    .filter(el => el[1] > 0)
    .reverse(),
  asks: [
    [57517.5, 10100.0],
    [57520.0, 0.0],
    [57520.5, 2500.0],
    [57523.0, 20000.0],
    [57550.0, 16510.0],
    [57586.0, 2578.0],
    [57587.0, 399999.0],
    [57587.5, 116999.0],
    [57590.5, 0.0],
    [57591.0, 0.0],
    [57608.5, 6681.0],
    [57679.0, 7866.0],
    [57760.5, 9050.0],
    [57839.5, 12948.0],
    [58038.0, 17539.0],
  ]
    .filter(el => el[1] > 0)
    .reverse(),
  product_id: 'PI_XBTUSD',
  tick: 0.5,
  tick_size: 1,
};
*/

const initialState: DataState = {
  bids: [],
  asks: [],
  product_id: 'PI_XBTUSD',
  tick: 0.5,
  tick_size: 1,
};

export const dataSlice = createSlice({
  name: 'data',
  initialState,
  reducers: {
    /**
     * Used for update & initial setup
     */
    updateOrders: (state, action: PayloadAction<TDataMessage>) => {
      if (action.payload.asks.length) {
        if (state.asks.length) {
          const incomingPrices = action.payload.asks.map(el => el[0]);
          state.asks = state.asks.filter(
            statePrice => !incomingPrices.includes(statePrice[0]),
          );
        }
        state.asks = [...state.asks, ...action.payload.asks]
          .filter(price => price[1])
          .sort((prev, next) => next[0] - prev[0]);
      }

      if (action.payload.bids.length) {
        if (state.bids.length) {
          const incomingPrices = action.payload.bids.map(el => el[0]);
          state.bids = state.bids.filter(
            statePrice => !incomingPrices.includes(statePrice[0]),
          );
        }
        state.bids = [...state.bids, ...action.payload.bids]
          .filter(price => price[1])
          .sort((prev, next) => next[0] - prev[0]);
      }
    },

    clearProduct: state => {
      state.product_id = null;
    },

    setTick: (state, action: PayloadAction<TTick>) => {
      state.tick = action.payload;
    },

    setProduct: (
      state,
      action: PayloadAction<{product_id: TProduct; tick_size: TTickSize}>,
    ) => {
      state.product_id = action.payload.product_id;
      state.tick_size = action.payload.tick_size;
      state.asks = [];
      state.bids = [];
    },
  },
});

export const {updateOrders, clearProduct, setTick, setProduct} =
  dataSlice.actions;

export default dataSlice.reducer;
