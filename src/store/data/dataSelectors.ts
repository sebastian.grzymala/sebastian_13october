import {shallowEqual} from 'react-redux';
import {createSelector} from 'reselect';
import {RootState} from '../store';
import {useAppSelector} from '../../hooks';
import reduceDuplicatedPrices from '../../utils/reduceDuplicatedPrices';
import tickPrice from '../../utils/tickPrice';

export const selectBids = (state: RootState) => state.data.bids;
export const selectAsks = (state: RootState) => state.data.asks;
export const selectTick = (state: RootState) => state.data.tick;
export const selectTickSize = (state: RootState) => state.data.tick_size;
export const selectProductId = (state: RootState) => state.data.product_id;

const selectAsksWithTick = createSelector(
  [selectAsks, selectTick, selectTickSize],
  (asks, tick, tick_size) => {
    if (!tick) {
      return asks;
    }
    return reduceDuplicatedPrices(
      asks.map(ask => [
        tickPrice(ask[0] * tick_size, tick) / tick_size,
        ask[1],
      ]),
    );
  },
);

const selectBidsWithTick = createSelector(
  [selectBids, selectTick, selectTickSize],
  (bids, tick, tick_size) => {
    if (!tick) {
      return bids;
    }
    return reduceDuplicatedPrices(
      bids.map(bid => [
        tickPrice(bid[0] * tick_size, tick) / tick_size,
        bid[1],
      ]),
    );
  },
);

export const selectAllPrices = createSelector(
  selectAsksWithTick,
  selectBidsWithTick,
  (rawAsks, rawBids) => {
    let totalAskSum = 0;
    let totalBidSum = 0;

    const asksLength = rawAsks.length;
    const bidsLength = rawBids.length;

    type TDetailedItem = {
      value: number;
      size: number;
      total: number;
    };

    let asks: ({
      type: 'ASK';
    } & TDetailedItem)[] = [];

    let bids: ({
      type: 'BID';
    } & TDetailedItem)[] = [];

    for (let i = 0; i < bidsLength; i++) {
      const [value, size] = rawBids[i];
      totalBidSum += size;
      bids[i] = {type: 'BID', value, size, total: totalBidSum};
    }

    for (let i = asksLength - 1; i >= 0; i--) {
      const [value, size] = rawAsks[i];
      totalAskSum += size;
      asks[i] = {type: 'ASK', value, size, total: totalAskSum};
    }
    return [...asks, ...bids];
  },
);

export const selectAskTotalSum = createSelector([selectAsks], asks =>
  asks.reduce((acc: number, item) => acc + item[1], 0),
);

export const selectBidTotalSum = createSelector([selectBids], bids =>
  bids.reduce((acc: number, item) => acc + item[1], 0),
);

export const useSelectBidPrices = () => {
  return useAppSelector(
    state => selectBidsWithTick(state).map(el => el[0]),
    shallowEqual,
  );
};

export const useSelectAskPrices = () => {
  return useAppSelector(
    state => selectAsksWithTick(state).map(el => el[0]),
    shallowEqual,
  );
};

export const useSelectAskTotalSum = () => {
  return useAppSelector(state => selectAskTotalSum(state));
};

export const useSelectBidTotalSum = () => {
  return useAppSelector(state => selectBidTotalSum(state));
};

export const useSelectTick = () => {
  return useAppSelector(state => selectTick(state));
};

export const useSelectTickSize = () => {
  return useAppSelector(state => selectTickSize(state));
};

export const useSelectProductId = () => {
  return useAppSelector(state => selectProductId(state));
};

export const useSelectAllPrices = () => {
  return useAppSelector(state => selectAllPrices(state));
};
