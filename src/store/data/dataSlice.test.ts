import dataReducer, {
  updateOrders,
  clearProduct,
  setProduct,
  setTick,
} from './dataSlice';

type TPriceList = [number, number][];

test('should return the initial state', () => {
  expect(dataReducer(undefined, {type: ''})).toEqual({
    bids: [],
    asks: [],
    product_id: 'PI_XBTUSD',
    tick: 0.5,
    tick_size: 1,
  });
});

test('For empty state should add bids & asks ordered by price from starting from highest', () => {
  const payload = {
    asks: [
      [100, 1],
      [500, 1],
      [200, 1],
    ] as TPriceList,
    bids: [
      [100, 2],
      [70, 2],
      [80, 2],
      [90, 2],
    ] as TPriceList,
  };

  expect(
    dataReducer(
      {
        product_id: 'PI_XBTUSD' as const,
        asks: [] as TPriceList,
        bids: [] as TPriceList,
        tick: 1,
        tick_size: 1,
      },
      updateOrders(payload),
    ),
  ).toEqual({
    product_id: 'PI_XBTUSD',
    asks: [
      [500, 1],
      [200, 1],
      [100, 1],
    ] as TPriceList,
    bids: [
      [100, 2],
      [90, 2],
      [80, 2],
      [70, 2],
    ] as TPriceList,
    tick: 1,
    tick_size: 1,
  });
});

test('If asks or bids contains 0 size values then remove them from the list', () => {
  const payload = {
    asks: [[200, 0]] as TPriceList,
    bids: [[100, 0]] as TPriceList,
  };

  expect(
    dataReducer(
      {
        product_id: 'PI_XBTUSD',
        asks: [
          [500, 1],
          [200, 1],
          [100, 1],
        ] as TPriceList,
        bids: [
          [100, 2],
          [90, 2],
          [80, 2],
        ] as TPriceList,
        tick: 1,
        tick_size: 1,
      },
      updateOrders(payload),
    ),
  ).toEqual({
    product_id: 'PI_XBTUSD',
    asks: [
      [500, 1],
      [100, 1],
    ] as TPriceList,
    bids: [
      [90, 2],
      [80, 2],
    ] as TPriceList,
    tick: 1,
    tick_size: 1,
  });
});

test('Clear data when we unsubscribe', () => {
  expect(
    dataReducer(
      {
        product_id: 'PI_XBTUSD',
        asks: [[500, 1]] as TPriceList,
        bids: [[100, 2]] as TPriceList,
        tick: 1,
        tick_size: 1,
      },
      clearProduct(),
    ),
  ).toEqual({
    product_id: null,
    asks: [[500, 1]] as TPriceList,
    bids: [[100, 2]] as TPriceList,
    tick: 1,
    tick_size: 1,
  });
});

test('Set product name & tick_size', () => {
  expect(
    dataReducer(
      {
        product_id: 'PI_XBTUSD',
        asks: [],
        bids: [],
        tick: 1,
        tick_size: 1,
      },
      setProduct({product_id: 'PI_ETHUSD', tick_size: 10}),
    ),
  ).toEqual({
    product_id: 'PI_ETHUSD',
    asks: [],
    bids: [],
    tick: 1,
    tick_size: 10,
  });
});

test('Set tick', () => {
  expect(
    dataReducer(
      {
        product_id: 'PI_XBTUSD',
        asks: [],
        bids: [],
        tick: 1,
        tick_size: 1,
      },
      setTick(2.5),
    ),
  ).toEqual({
    product_id: 'PI_XBTUSD',
    asks: [],
    bids: [],
    tick: 2.5,
    tick_size: 1,
  });
});
