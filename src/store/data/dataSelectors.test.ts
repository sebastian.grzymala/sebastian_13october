import 'react-native';
import {RootState} from '../store';

import {selectAsks, selectAskTotalSum, selectAllPrices} from './dataSelectors';

const state = {
  data: {
    asks: [
      [48341, 2300],
      [48340, 500],
      [48339, 400],
    ],
    bids: [
      [47339, 1000],
      [47340, 900],
      [47341, 800],
    ],
    product_id: null,
    tick: 1,
    tick_size: 1,
  },
  ws: {connectionState: 'CLOSED'},
} as RootState;

test('select ask ids', () => {
  expect(selectAsks(state)).toEqual([
    [48341, 2300],
    [48340, 500],
    [48339, 400],
  ]);
});

test('select ask total sum', () => {
  expect(selectAskTotalSum(state)).toEqual(3200);
});

test('selectAllPrices selector should build array with valir properties & totalsum counted', () => {
  const state2 = {
    data: {
      asks: [
        [500, 50],
        [400, 40],
      ],
      bids: [
        [300, 30],
        [200, 20],
      ],
      product_id: null,
      tick: 1,
      tick_size: 1,
    },
    ws: {connectionState: 'CLOSED'},
  } as RootState;

  expect(selectAllPrices(state2)).toEqual([
    {
      type: 'ASK',
      value: 500,
      size: 50,
      total: 90,
    },
    {
      type: 'ASK',
      value: 400,
      size: 40,
      total: 40,
    },
    {
      type: 'BID',
      value: 300,
      size: 30,
      total: 30,
    },
    {
      type: 'BID',
      value: 200,
      size: 20,
      total: 50,
    },
  ]);
});
