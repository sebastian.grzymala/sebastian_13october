import {configureStore} from '@reduxjs/toolkit';
import dataReducer from './data/dataSlice';
import wsReducer from './ws/wsSlice';

export const store = configureStore({
  reducer: {
    data: dataReducer,
    ws: wsReducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
