import {RootState} from '../store';
import {useAppSelector} from '../../hooks';
export const selectConnectionState = (state: RootState) =>
  state.ws.connectionState;

export const useSelectConnectionState = () => {
  return useAppSelector(state => selectConnectionState(state));
};
