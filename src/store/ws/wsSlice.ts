import {createSlice, PayloadAction} from '@reduxjs/toolkit';

type TConnectionState = 'CLOSED' | 'PENDING' | 'OPEN';

export interface DataState {
  connectionState: TConnectionState;
}

const initialState: DataState = {
  connectionState: 'CLOSED',
};

export const wsSlice = createSlice({
  name: 'connection',
  initialState,
  reducers: {
    setConnectionState: (state, action: PayloadAction<TConnectionState>) => {
      state.connectionState = action.payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const {setConnectionState} = wsSlice.actions;

export default wsSlice.reducer;
