import WS from './WsHandler';

type TProduct = 'PI_XBTUSD' | 'PI_ETHUSD';

export const subscribe = (product: TProduct) => {
  WS.send({event: 'subscribe', feed: 'book_ui_1', product_ids: [product]});
};

export const unSubscribe = (product: TProduct) => {
  WS.send({event: 'unsubscribe', feed: 'book_ui_1', product_ids: [product]});
};
