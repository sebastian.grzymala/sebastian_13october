import {WS_ENDPOINT} from '../../env';
import {store} from '../store/store';
import {setConnectionState} from '../store/ws/wsSlice';
import {updateOrders} from '../store/data/dataSlice';
import {
  isPriceMessage,
  TIncomingPricesMessage,
  TIncomingMessage,
} from './IncomingMessage';

const RECONNECT_ON_ERROR_SECONDS = 1;

const getEmptyDelta = () => ({
  asks: new Map<number, number>(),
  bids: new Map<number, number>(),
});

type TpriceList = [number, number][];

class WsHandler {
  private ws: WebSocket | null = null;
  private reconnectInterval: ReturnType<typeof setInterval> | null = null;

  private deltaBatch = getEmptyDelta();

  public connect = () => {
    //console.log('Hello connect!');
    /*
    if (this.ws && this.ws.readyState !== WebSocket.CLOSED) {
      return;
    }
    */
    this.ws = new WebSocket(WS_ENDPOINT);
    this.ws.onopen = this.onOpen;
    this.ws.onerror = this.onError;
    this.ws.onclose = this.onClose;
    this.ws.onmessage = this.onMessage;
  };

  private onError = () => {
    this.ws?.close();
    store.dispatch(setConnectionState('PENDING'));
  };

  private onOpen = async () => {
    if (this.reconnectInterval) {
      clearInterval(this.reconnectInterval);
      this.reconnectInterval = null;
    }
    store.dispatch(setConnectionState('OPEN'));
  };

  private onClose = async () => {
    if (this.reconnectInterval) {
      clearInterval(this.reconnectInterval);
      this.reconnectInterval = null;
    }
    store.dispatch(setConnectionState('PENDING'));
    this.reconnect();
  };

  private reconnect = () => {
    if (this.reconnectInterval) {
      return null;
    }
    this.reconnectInterval = setInterval(() => {
      this.connect();
    }, RECONNECT_ON_ERROR_SECONDS * 1000);
  };

  private onMessage = (message: {data?: TIncomingMessage}) => {
    //console.log('message arrived', message.data);
    if (!message.data) {
      return;
    }
    const dataObj = JSON.parse(message.data);
    if (dataObj && isPriceMessage(dataObj)) {
      // quick fix for delayed messages:
      const state = store.getState();
      if (state.data.product_id !== dataObj.product_id) {
        return;
      }
      this.feedDeltaBatch(dataObj as TIncomingPricesMessage);
    }
  };

  public send = (message: unknown) => {
    if (this.ws && this.ws.readyState === WebSocket.OPEN) {
      this.clearDeltaBatch();
      this.ws?.send(JSON.stringify(message));
    }
  };

  /* Delta batch control */
  private feedDeltaBatch = ({
    asks,
    bids,
  }: {
    asks: TpriceList;
    bids: TpriceList;
  }) => {
    asks.forEach(ask => this.deltaBatch.asks.set(ask[0], ask[1]));
    bids.forEach(bid => this.deltaBatch.bids.set(bid[0], bid[1]));
    requestAnimationFrame(() => this.storeUpdateOrders());
  };

  private clearDeltaBatch = () => (this.deltaBatch = getEmptyDelta());

  private storeUpdateOrders = () => {
    store.dispatch(
      updateOrders({
        asks: Array.from(this.deltaBatch.asks.entries()),
        bids: Array.from(this.deltaBatch.bids.entries()),
      }),
    );
    this.clearDeltaBatch();
  };
}

const WsHandlerInstance = new WsHandler();

export default WsHandlerInstance;
