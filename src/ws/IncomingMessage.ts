type TPriceList = [number, number][];

export type TIncomingPricesMessage = {
  bids: TPriceList;
  asks: TPriceList;
  product_id: 'PI_XBTUSD' | 'PI_ETHUSD';
};

export type TIncomingMessage = TIncomingPricesMessage | any;

export const isPriceMessage = (
  data: unknown & {asks?: TPriceList; bids?: TPriceList},
) => {
  return typeof data.asks !== 'undefined' && typeof data.bids !== 'undefined';
};
