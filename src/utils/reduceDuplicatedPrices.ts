export type TpriceList = [number, number][];

const reduceDuplicatedPrices = (prices: TpriceList) => {
  const reduced: TpriceList = [];
  let reducedIndex = -1;

  prices.forEach((price, index) => {
    if (index !== 0 && price[0] === prices[index - 1][0]) {
      reduced[reducedIndex][1] += price[1];
    } else {
      reduced.push(price);
      reducedIndex++;
    }
  });
  return reduced;
};

export default reduceDuplicatedPrices;
