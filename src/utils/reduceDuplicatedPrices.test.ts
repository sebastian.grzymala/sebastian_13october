import reduceDuplicatedPrices, {TpriceList} from './reduceDuplicatedPrices';

const prices = [
  [48339, 400],
  [48340, 500],
  [48340, 50],
  [48340, 5],
  [48341, 2300],
  [48341, 700],
] as TpriceList;

test('reduce array elements when dupiacate on 1st by sum app 2nd value', () => {
  expect(reduceDuplicatedPrices(prices)).toEqual([
    [48339, 400],
    [48340, 555],
    [48341, 3000],
  ]);
});
