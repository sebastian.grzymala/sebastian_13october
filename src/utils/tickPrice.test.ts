import tickPrice from './tickPrice';

test('should return the incoming value if tick is null', () => {
  expect(tickPrice(1.234567, null)).toEqual(1.234567);
});

test('should return integer if tick is 1', () => {
  expect(tickPrice(7.234567, 1)).toEqual(7);
});

test('should multiples 2.5 if tick is 2.5: value 9.234567', () => {
  expect(tickPrice(9.234567, 2.5)).toEqual(7.5);
});

test('should multiples 2.5 if tick is 2.5: value 7.5', () => {
  expect(tickPrice(7.5, 2.5)).toEqual(7.5);
});

test('should multiples 2.5 if tick is 2.5: value 0', () => {
  expect(tickPrice(0, 2.5)).toEqual(0);
});
