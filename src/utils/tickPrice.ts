type Ttick = 0.5 | 1 | 2.5;

const tickPrice = (price: number, tick: Ttick) => {
  if (tick === 1) {
    return Math.floor(price);
  }
  if (tick === 2.5) {
    return Math.floor(price * 0.4) * 2.5;
  }
  return price;
};

export default tickPrice;
