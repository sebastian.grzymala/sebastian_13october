const colors = {
  BUY: 'green',
  SELL: 'red',
  textColor: 'white',
  textColorSecondary: 'silver',
  backgroundColor: 'black',
} as const;

export default colors;
