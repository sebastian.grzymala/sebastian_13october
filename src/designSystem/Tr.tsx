import React from 'react';
import {View, StyleSheet} from 'react-native';

/**
 * Table row component
 */
const Tr: React.FC<{}> = ({children}) => (
  <View style={styles.tr}>{children}</View>
);

const styles = StyleSheet.create({
  tr: {
    paddingVertical: 7.5,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
});

export default Tr;
