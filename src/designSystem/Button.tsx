import React from 'react';
import {TouchableOpacity, Text, StyleSheet} from 'react-native';
import colors from '../designSystem/Color';

interface IButton {
  type: 'TOGGLE_FEED' | 'ON_OFF';
  label: string;
  disabled?: boolean;
  onPress: () => void;
}

const Button: React.FC<IButton> = ({type, label, onPress, disabled}) => (
  <TouchableOpacity
    onPress={onPress}
    disabled={disabled}
    style={[
      styles.button,
      type === 'ON_OFF' ? styles.button_red : styles.button_purple,
      disabled ? styles.disabled : {},
    ]}>
    <Text style={styles.text}>{label}</Text>
  </TouchableOpacity>
);

export default Button;

const styles = StyleSheet.create({
  button: {
    paddingHorizontal: 15,
    justifyContent: 'center',
    marginHorizontal: 7.5,
    height: 30,
    borderRadius: 3,
  },
  button_purple: {
    backgroundColor: 'rgb(88,72,214)',
  },
  button_red: {
    backgroundColor: 'rgb(183,32,36)',
  },
  disabled: {
    opacity: 0.3,
  },
  text: {
    color: colors.textColor,
  },
});
