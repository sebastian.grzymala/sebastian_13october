import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import colors from './Color';

interface ITd {
  color?: string;
}
/**
 * Table cell cmponent
 */
const Td: React.FC<ITd> = ({color, children}) => (
  <View style={styles.wrapper}>
    <Text style={[styles.text, color ? {color} : {}]}>{children}</Text>
  </View>
);

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    paddingRight: 30,
  },
  text: {textAlign: 'right', color: colors.textColor},
});

export default Td;
