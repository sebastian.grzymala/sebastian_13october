import React from 'react';
import {View, StyleSheet, SafeAreaView} from 'react-native';
import colors from '../designSystem/Color';
import Td from '../designSystem/Td';
import Tr from '../designSystem/Tr';
import PriceList from './PriceList';
import FeedControl from './FeedControl';
import Header from './Header';
import Footer from './Footer';

const OrderBook: React.FC<{}> = () => {
  return (
    <View style={styles.wrapper}>
      <SafeAreaView style={styles.wrapper}>
        <Header />
        <Tr>
          <Td color={colors.textColorSecondary}>PRICE</Td>
          <Td color={colors.textColorSecondary}>SIZE</Td>
          <Td color={colors.textColorSecondary}>TOTAL</Td>
        </Tr>
        <PriceList />
        <Footer />
      </SafeAreaView>
      <FeedControl />
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: colors.backgroundColor,
  },
  titleContainer: {
    paddingHorizontal: 30,
    paddingVertical: 15,
    borderBottomWidth: 2,
    borderBottomColor: 'silver',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  titleText: {
    color: colors.textColor,
    fontSize: 20,
  },
  sep: {
    height: 30,
  },
  list: {
    flexGrow: 2,
  },
});

export default OrderBook;
