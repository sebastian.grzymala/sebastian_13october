import React, {useEffect, useMemo, useRef} from 'react';
import {StyleSheet, Dimensions, Animated} from 'react-native';
import colors from '../designSystem/Color';

import {
  useSelectAskTotalSum,
  useSelectBidTotalSum,
} from '../store/data/dataSelectors';

interface IDepth {
  total: number;
}

interface IDepthView {
  depthRatio: number;
  priceType: 'ASK' | 'BID';
}

const screenWidth = Dimensions.get('screen').width;

export const DepthAsk: React.FC<IDepth> = ({total}) => {
  const totalSum = useSelectAskTotalSum();
  const depthRatio = useMemo(() => total / totalSum, [total, totalSum]);
  return <DepthView depthRatio={depthRatio} priceType={'ASK'} />;
};

export const DepthBid: React.FC<IDepth> = ({total}) => {
  const totalSum = useSelectBidTotalSum();
  const depthRatio = useMemo(() => total / totalSum, [total, totalSum]);
  return <DepthView depthRatio={depthRatio} priceType={'BID'} />;
};

const depthRatioToOffset = (depthRatio: number) =>
  screenWidth * (1 - depthRatio);

const DepthView: React.FC<IDepthView> = ({priceType, depthRatio}) => {
  const offsetAnim = useRef(new Animated.Value(depthRatioToOffset(depthRatio)));

  useEffect(() => {
    Animated.spring(offsetAnim.current, {
      toValue: depthRatioToOffset(depthRatio),
      useNativeDriver: true,
    }).start();
  }, [depthRatio]);

  return (
    <Animated.View
      style={[
        styles.depth,
        priceType === 'ASK' ? styles.depth_ask : styles.depth_bid,
        {
          transform: [{translateX: offsetAnim.current}],
        },
      ]}
    />
  );
};

const styles = StyleSheet.create({
  depth: {
    opacity: 0.3,
    ...StyleSheet.absoluteFillObject,
  },
  depth_ask: {
    backgroundColor: colors.BUY,
  },
  depth_bid: {
    backgroundColor: colors.SELL,
  },
});
