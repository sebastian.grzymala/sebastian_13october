import React from 'react';
import {View, StyleSheet, Text} from 'react-native';
import TickPicker from './TickPicker';
import colors from '../designSystem/Color';

const Header: React.FC = () => (
  <View style={styles.titleContainer}>
    <Text style={styles.titleText}>Order Book</Text>
    <TickPicker />
  </View>
);

const styles = StyleSheet.create({
  titleContainer: {
    paddingHorizontal: 30,
    paddingVertical: 15,
    borderBottomWidth: 2,
    borderBottomColor: 'silver',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  titleText: {
    color: colors.textColor,
    fontSize: 20,
  },
});

export default Header;
