import React, {useCallback} from 'react';
import {View, StyleSheet} from 'react-native';
import Button from '../designSystem/Button';

import {useDispatch} from 'react-redux';

import {useSelectProductId} from '../store/data/dataSelectors';
import {clearProduct, setProduct} from '../store/data/dataSlice';
import {subscribe, unSubscribe} from '../ws/Messages';

const Footer: React.FC = () => {
  const dispatch = useDispatch();
  const product_id = useSelectProductId();

  const onFeedOnOffPressed = useCallback(() => {
    if (product_id) {
      dispatch(clearProduct());
      unSubscribe(product_id);
    } else {
      dispatch(setProduct({product_id: 'PI_XBTUSD', tick_size: 1}));
      subscribe('PI_XBTUSD');
    }
  }, [product_id, dispatch]);

  const onFeedTogglePresses = useCallback(() => {
    if (!product_id) {
      return;
    }
    dispatch(clearProduct());
    unSubscribe(product_id);
    const new_product_id =
      product_id === 'PI_XBTUSD' ? 'PI_ETHUSD' : 'PI_XBTUSD';
    const new_tick_size = new_product_id === 'PI_XBTUSD' ? 1 : 10;
    dispatch(
      setProduct({product_id: new_product_id, tick_size: new_tick_size}),
    );
    subscribe(new_product_id);
  }, [product_id, dispatch]);

  return (
    <View style={styles.conatiner}>
      <Button
        label="Toggle Feed"
        onPress={onFeedTogglePresses}
        type="TOGGLE_FEED"
        disabled={!product_id}
      />
      <Button
        label={product_id ? 'Kill feed' : 'Start feed'}
        onPress={onFeedOnOffPressed}
        type="ON_OFF"
      />
    </View>
  );
};

const styles = StyleSheet.create({
  conatiner: {
    padding: 30,
    flexDirection: 'row',
    justifyContent: 'center',
  },
});

export default Footer;
