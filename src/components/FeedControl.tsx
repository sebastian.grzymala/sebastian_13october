import React, {useEffect, useRef} from 'react';
import {subscribe} from '../ws/Messages';
import {connect} from '../ws/connect';
import {useSelectConnectionState} from '../store/ws/wsSelectors';
import {useSelectProductId} from '../store/data/dataSelectors';

// Feed autostart when connection created / reopened

const FeedControl: React.FC<{}> = () => {
  const connectionState = useSelectConnectionState();
  const productId = useSelectProductId();

  const connectionStateRef = useRef<typeof connectionState>();
  const productIdRef = useRef<typeof productId>();

  // initial connect
  useEffect(() => connect(), []);

  // subscribe on ws open if product id definied
  useEffect(() => {
    if (connectionState === 'OPEN') {
      if (productIdRef.current) {
        subscribe(productIdRef.current);
      }
    }
  }, [connectionState]);

  productIdRef.current = productId;
  connectionStateRef.current = connectionState;

  return null;
};

export default FeedControl;
