import React from 'react';
import {View, StyleSheet} from 'react-native';
import {useDispatch} from 'react-redux';
import RNPickerSelect from 'react-native-picker-select';

import {useSelectTick, useSelectTickSize} from '../store/data/dataSelectors';
import {setTick} from '../store/data/dataSlice';

const TickPicker: React.FC<{}> = () => {
  const tick = useSelectTick();
  const tick_size = useSelectTickSize();
  const dispatch = useDispatch();

  return (
    <View style={styles.wrapper}>
      <RNPickerSelect
        value={tick}
        onValueChange={itemValue => dispatch(setTick(itemValue))}
        items={[
          {label: `Group ${0.5 / tick_size}`, value: 0.5},
          {label: `Group ${1 / tick_size}`, value: 1},
          {label: `Group ${2.5 / tick_size}`, value: 2.5},
        ]}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: 'grey',
    padding: 5,
    borderRadius: 5,
  },
});

export default TickPicker;
