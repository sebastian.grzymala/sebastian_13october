import React from 'react';
import {FlatList} from 'react-native';
import {PriceItem} from './PriceItem';
import {useSelectAllPrices} from '../store/data/dataSelectors';

const PriceList = () => {
  const prices = useSelectAllPrices();
  return (
    <FlatList
      data={prices}
      renderItem={({item}) => (
        <PriceItem
          priceType={item.type}
          price={item.value}
          size={item.size}
          total={item.total}
        />
      )}
      keyExtractor={item => item.type + '-' + item.value}
    />
  );
};

export default PriceList;
