import React from 'react';
import {View, StyleSheet} from 'react-native';

import colors from '../designSystem/Color';
import Td from '../designSystem/Td';
import Tr from '../designSystem/Tr';
import {DepthAsk, DepthBid} from './Depth';

import {useSelectTickSize} from '../store/data/dataSelectors';

interface IPriceItem {
  price: number;
}

interface IPriceItemView {
  priceType: 'ASK' | 'BID';
  price: number;
  size: number;
  total: number;
}

const priceFormated = (price: number, tick_size: number) =>
  price.toLocaleString('en-US', {
    minimumFractionDigits: tick_size === 1 ? 1 : 2,
  });

const numberFormated = (numberValue: number) => numberValue.toLocaleString();

export const PriceItem: React.FC<IPriceItemView> = ({
  priceType,
  price,
  size,
  total,
}) => {
  const tick_size = useSelectTickSize();

  return (
    <View style={styles.wrapper}>
      {priceType === 'ASK' ? (
        <DepthAsk total={total} />
      ) : (
        <DepthBid total={total} />
      )}
      <Tr>
        <Td color={priceType === 'ASK' ? colors.BUY : colors.SELL}>
          {priceFormated(price, tick_size)}
        </Td>
        <Td>{numberFormated(size)}</Td>
        <Td>{numberFormated(total)}</Td>
      </Tr>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    position:
      'relative' /* we want this as Depth element is positioned absolutly */,
  },
});
