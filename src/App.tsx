import React from 'react';
import {Provider} from 'react-redux';
import {store} from './store/store';
import OrderBook from './components/OrderBook';

const App: React.FC<{}> = () => (
  <Provider store={store}>
    <OrderBook />
  </Provider>
);

export default App;
