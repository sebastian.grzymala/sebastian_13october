# The Orderbook
A simple app written in React Native Typescript fed by a heavy load of websocket messages.

In this video, you can see it running with a performence monitor (in debug mode) on physical iPhone 12 mini: https://www.youtube.com/watch?v=rGpS98jeb3g (video is unlisted)

Take notice of wifi on/off behavior and depth visualizer animations.